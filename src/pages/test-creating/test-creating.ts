import {Component, OnInit} from '@angular/core';
import {AlertController, NavController, NavParams} from 'ionic-angular';
import {Answer, Question, QuestionType, Test} from "../../models/test";
import {TestsDaoService} from "../../providers/dao/tests.dao.service";
import {FormArray, FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import {TestPassPage} from "../test-pass/test-pass";

/**
 * Generated class for the TestCreatingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-test-creating',
  templateUrl: 'test-creating.html',
})
export class TestCreatingPage implements OnInit {

  testForm: FormGroup;
  private id: number;
  test: Test;
  questionType = {
    multi: QuestionType.MULTI,
    single: QuestionType.SINGLE
  };
  submitPressed: boolean = false;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private formBuilder: FormBuilder,
              private testDaoService: TestsDaoService) {
  }

  ngOnInit(){
    this.id = this.navParams.data.id;
    this.getTest(this.id);
  }

  getTest(id?) {
    this.testDaoService.getTest(id).subscribe(
      (res: Test) => {
        if (!res.questions) {res.questions = []}
        this.test = res;
        res ? this.createTestForm(res) : this.goBack();
      }
    );
  }

  goBack(){
    this.navCtrl.push(this.navCtrl.getPrevious().component, null, {direction: 'back'})
  }

  createTestForm(test: Test){
    this.testForm = this.formBuilder.group({
      "name": [test.name, [Validators.required]],
      "minCorrectAnswers": [test.minCorrectAnswers],
      "questions": this.formBuilder.array([])
    });

    test.questions.forEach((question: Question) => {
      this.createQuestion(question)
    })
  }
  createQuestion(question?: Question){
    const questionToAdd: Question = question ? question : {
      questionText: '',
      questionType: this.questionType.single,
      answers: [{
        body: '',
        correctness: false
      }]
    };
    const formArray = <FormArray>this.testForm.controls["questions"];
    formArray.push(this.formBuilder.group({
      "questionText": [questionToAdd.questionText, [Validators.required]],
      "questionType": [questionToAdd.questionType],
      "answers": this.formBuilder.array([])
    }));

    questionToAdd.answers.forEach((answer: Answer) => {
      let formGroup: any = formArray.controls;
      this.createAnswer(formGroup[formGroup.length - 1], question ? answer : null)
    })
  }

  createAnswer(questionForm: FormGroup, answer?: Answer){
    const questionToAdd = answer ? answer : {
      body: '',
      correctness: false
    };
    const formArray = questionForm ? <FormArray>questionForm.controls["answers"] : this.formBuilder.array([]);
    if (!answer) {
      questionToAdd.correctness = formArray.length == 0;
    }
    formArray.push(this.formBuilder.group({
      "body": [questionToAdd.body, [Validators.required]],
      "correctness": [questionToAdd.correctness]
    }));
  }

  submitTest(testForm: NgForm){
    this.submitPressed = true;
    if (testForm.invalid) return;
    this.testDaoService.updateTest(Object.assign({
      id: this.id,
      dateAdded: this.test.dateAdded,
      questionsCount: testForm.value.questions.length
    }, testForm.value)).subscribe(
      res => {
        this.test = res;
        this.submitPressed = false;
      }
    )
  }

  changeQuestionType(indexOfQuestion: number){
    const formArray: any = <FormArray>this.testForm.controls['questions'],
      question: any = formArray.controls[indexOfQuestion],
      answers: any = question.controls['answers'].controls,
      questionType = question.controls['questionType'].value;

    if (questionType == QuestionType.SINGLE) {
      let countOfTruthy: number = answers.filter(answer => answer.value.correctness).length;
      if (countOfTruthy != 1) this.changeRadio(indexOfQuestion, 0)
    }
  }

  changeRadio(indexOfQuestion, indexShouldBeEquallTo, status?: boolean){
    const formArray: any = <FormArray>this.testForm.controls['questions'],
      question: any = formArray.controls[indexOfQuestion],
      answers: any = question.controls['answers'].controls,
      questionType = question.controls['questionType'].value;

    if (questionType == QuestionType.SINGLE) {
      answers.forEach((answerItem, index) => {
        answerItem.controls.correctness.setValue(index == indexShouldBeEquallTo);
      })
    } else {
      answers[indexShouldBeEquallTo].controls.correctness.setValue(status);
    }
  }

  removeAnswer(questionForm: FormGroup, index: number){
    const alert = this.alertCtrl.create({
      title: `Are you sure you want to remove this answer?`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Skip modal');
          }
        },
        {
          text: 'Remove',
          handler: data => {
            const formArray = <FormArray>questionForm.controls["answers"];
            formArray.removeAt(index)
          }
        }
      ]
    });
    alert.present();
  }

  removeQuestion(index: number){
    const alert = this.alertCtrl.create({
      title: `Are you sure you want to remove this question?`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Skip modal');
          }
        },
        {
          text: 'Remove',
          handler: data => {
            const formArray = <FormArray>this.testForm.controls["questions"];
            formArray.removeAt(index)
          }
        }
      ]
    });
    alert.present();
  }

  get modelWasChanged(): boolean{
    const initialTest: any = Object.assign({}, this.test);
    delete initialTest.dateAdded;
    delete initialTest.id;
    delete initialTest.questionsCount;
    return JSON.stringify(this.testForm.value) != JSON.stringify(initialTest) || !this.testForm;
  }

  openTestToPass(): void {
    this.navCtrl.push(TestPassPage, {id: this.id});
  }


  ionViewDidLoad() {
    console.log(`ionViewDidLoad TestCreatingPage`, this.navParams.data);
  }

  checkQuestionText(question): boolean{
    return question.controls['questionText'].invalid && (question.controls['questionText'].touched || this.submitPressed)
  }

  checkAnswerText(answer): boolean{
    return answer.controls['body'].invalid && (answer.controls['body'].touched || this.submitPressed)
  }

}
