import {Component} from "@angular/core";
import {NavController, MenuController} from "ionic-angular";
import {HomePage} from "../home/home";
import {FacebookLoginResponse} from "@ionic-native/facebook";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  constructor(public nav: NavController,
              public menu: MenuController,
              public authServiceProvider: AuthServiceProvider) {
    this.menu.swipeEnable(false);
  }

  login() {
    this.nav.setRoot(HomePage);
  }

  loginWithFacebook() {
    this.authServiceProvider.login().subscribe(
      (res: FacebookLoginResponse) => {
        this.login();
      }
    );
  }


}
