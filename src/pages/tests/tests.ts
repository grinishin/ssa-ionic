import {ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {AlertController, NavController, PopoverController} from "ionic-angular";

import {NotificationsPage} from "../notifications/notifications";
import {SettingsPage} from "../settings/settings";
import {Process, Test} from "../../models/test";
import {TestsDaoService} from "../../providers/dao/tests.dao.service";
import {TestCreatingPage} from "../test-creating/test-creating";
import {TestPassPage} from "../test-pass/test-pass";


@Component({
  selector: 'page-home',
  templateUrl: 'tests.html'
})

export class TestsPage implements OnInit {

  private _modalOpenedForId: boolean;

  constructor(public nav: NavController,
              private alertCtrl: AlertController,
              private cd: ChangeDetectorRef,
              public testDaoService: TestsDaoService,
              public popoverCtrl: PopoverController) {
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    //
  }

  createTest() {
    const test: Test = {
      name: ``,
      dateAdded: '',
      questions: []
    };

    let id = 1;

    switch (this.testDaoService.tests.length) {
      case 0:
        id = 1;
        break;
      case 1:
        id = this.testDaoService.tests[0].id + 2;
        break;
      default:
        id = this.testDaoService.tests.sort((a, b) => {
          return a.id > b.id ? -1 : 1
        })[0].id + 2;
    }

    test.name = `Test ${id}`;
    this.openCreateModal(test, Process.CREATE)
  }

  // go to result page
  openCreateModal(test: Test, process?: Process) {
    this.refreshTestList(true);
    let processName = process == Process.CREATE ? 'Create' : 'Edit';
    const alert = this.alertCtrl.create({
      title: `${processName} test`,
      inputs: [
        {
          name: 'testName',
          placeholder: 'Test name',
          value: test.name
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Skip modal');
            this._modalOpenedForId = null;
          }
        },
        {
          text: processName,
          handler: data => {
            test.name = data.testName;
            if (process == Process.EDIT) {
              this.testDaoService.updateTest(test).subscribe(
                res => {
                  this.refreshTestList(true);
                }
              )
            } else {
              test.dateAdded = (new Date()).toDateString();
              this.testDaoService.createTest(test).subscribe(
                res => {
                  this.refreshTestList(true);
                }
              )
            }
            this._modalOpenedForId = null;
          }
        }
      ]
    });
    alert.present();
  }

  doRefresh(refresher){
    this.testDaoService.getTestList().subscribe(
      res => {
        refresher.complete();
      }
    );
  }

  openTest(test: Test){
    // console.log(this.nav)
    this.nav.push(TestCreatingPage, {id: test.id});
  }

  passTest(id: number){
    this.nav.push(TestPassPage, {id: id});
  }
  editTest(test: Test){
    if (!this._modalOpenedForId) {
      this._modalOpenedForId = Boolean(test);
      this.openCreateModal(test, Process.EDIT)
    }
  }

  removeTest(test: Test){
    let alert = this.alertCtrl.create({
      title: `Remove test ${test.name}`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Remove',
          handler: data => {
            this.testDaoService.deleteTest(test.id).subscribe()
          }
        }
      ]
    });
    alert.present();
  }

  refreshTestList(updateAgain?: boolean) {
    this.testDaoService.getTestList().subscribe(
      res => {
        this.cd.detectChanges();
        if (updateAgain) {
          setTimeout(()=>{
            this.refreshTestList()
          })
        }
      }
    );
  }

  // to go account page
  goToAccount() {
    this.nav.push(SettingsPage);
  }

  presentNotifications(myEvent) {
    console.log(myEvent);
    let popover = this.popoverCtrl.create(NotificationsPage);
    popover.present({
      ev: myEvent
    });
  }

}

//
