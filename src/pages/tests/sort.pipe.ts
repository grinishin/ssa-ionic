import {Pipe, PipeTransform} from "@angular/core";
import {Test} from "../../models/test";

@Pipe({name: 'sort'})
export class SortPipe implements PipeTransform {
  transform(tests: Test[]): Test[] {
    return tests.sort((test1, test2) => test1.name.toLowerCase() > test2.name.toLowerCase() ? 1 : -1)
  }
}
