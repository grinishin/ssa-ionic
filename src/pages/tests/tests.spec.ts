import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BrowserModule, By} from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { IonicModule, Platform, NavController} from 'ionic-angular/index';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {PlatformMock, SplashScreenMock, StatusBarMock} from "../../../test-config/mocks-ionic";
import {Facebook} from "@ionic-native/facebook";
import {StorageMock} from "../../mock/main-providers/storage.mock";
import {TestApiService} from "../../providers/api/test.api.service";
import {FacebookMock} from "../../mock/main-providers/facebook.mock";
import {Storage} from "@ionic/storage";
import {TestsDaoService} from "../../providers/dao/tests.dao.service";
import {TestsPage} from "./tests";
import {AlertController, PopoverController} from "ionic-angular";
import {HttpClientModule} from "@angular/common/http";
import {LongPressModule} from "ionic-long-press";
import {SortPipe} from "./sort.pipe";

describe('TestsPage ', () => {
  let de: DebugElement;
  let comp: TestsPage;
  let fixture: ComponentFixture<TestsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestsPage,

        SortPipe
      ],
      imports: [
        BrowserModule,
        LongPressModule,
        HttpClientModule,
        IonicModule.forRoot(TestsPage)
      ],
      providers: [
        NavController,
        TestApiService,
        TestsDaoService,
        AlertController,
        PopoverController,
        {provide: StatusBar, useClass: StatusBarMock},
        {provide: SplashScreen, useClass: SplashScreenMock},
        {provide: Platform, useClass: PlatformMock},
        {provide: Facebook, useClass: FacebookMock},
        {provide: Storage, useClass: StorageMock}
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestsPage);
    comp = fixture.componentInstance;
  });

  it('should create component', () => {
    expect(comp).toBeDefined()
  });

});
