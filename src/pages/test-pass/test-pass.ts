import {Component, OnInit} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {QuestionType, Test} from "../../models/test";
import {TestsDaoService} from "../../providers/dao/tests.dao.service";

/**
 * Generated class for the TestPassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-test-pass',
  templateUrl: 'test-pass.html',
})
export class TestPassPage implements OnInit {

  private id: number;
  test: Test;
  activeQuestionNumber: number = 0;
  answerModel: boolean[][] = [];
  questionType = {
    multi: QuestionType.MULTI,
    single: QuestionType.SINGLE
  };

  constructor(public navCtrl: NavController,
              private testDaoService: TestsDaoService,
              public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestPassPage');
  }

  ngOnInit() {
    this.id = this.navParams.data.id;
    this.getTest(this.id);
  }

  getTest(id?) {
    this.testDaoService.getTest(id).subscribe(
      (res: Test) => {
        if (!res) {
          this.goBack();
          return;
        }
        if (!res.questions) {
          res.questions = []
        }
        res ? this.createModel(res) : this.goBack();
      }
    );
  }

  goBack() {
    this.navCtrl.push(this.navCtrl.getPrevious().component, null, {direction: 'back'})
  }

  createModel(test: Test) {
    this.test = test;
    test.questions.forEach(question => {
      this.answerModel.push(question.answers.map((answer) => {
        return false
      }))
    });
  }

  get checkDisabling(): boolean{
    if (this.test.questions[this.activeQuestionNumber].questionType == QuestionType.SINGLE){
      let includedAnswers = this.answerModel[this.activeQuestionNumber].filter(answer => answer).length > 0;
      return !includedAnswers
    } else {
      return false
    }
  }

  submitAnswer() {
    this.activeQuestionNumber++;
    if (this.activeQuestionNumber < this.test.questions.length - 1) {

    } else {

    }
  }

  radioChanged(selectedIndex: number) {
    const answerModel = this.answerModel[this.activeQuestionNumber];
    answerModel.forEach((answer, index) => {
      answerModel[index] = index == selectedIndex;
    })
  }
}
