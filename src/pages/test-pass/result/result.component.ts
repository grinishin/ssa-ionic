import {Component, Input, OnInit} from '@angular/core';
import {Test} from "../../../models/test";

export interface TestResult {
  testLength: number;
  correctCount: number;
  wrongCount: number;
  correctPercent: string;
  passed: boolean;
}

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html'
})
export class ResultComponent implements OnInit {

  @Input() answers: boolean[][];
  @Input() test: Test;
  testResult: TestResult;

  constructor() {
  }

  ngOnInit() {
    this.testResult = this.calculateResults();
  }

  calculateResults(): TestResult {
    let wrongResult = this.test.questions.filter((question, indesQuestion) => {
      let answerResult = question.answers.filter((answer, indexAnswer) => {
        return this.answers[indesQuestion][indexAnswer] != answer.correctness
      });
      return answerResult.length > 0;
    });
    return {
      testLength: this.test.questions.length,
      correctCount: this.test.questions.length - wrongResult.length,
      wrongCount: wrongResult.length,
      correctPercent: `${(this.test.questions.length - wrongResult.length) / this.test.questions.length * 100}%`,
      passed: true
    }
  }

  get testResultKeys(){
    return Object.keys(this.testResult)
  }
}
