import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {LoginPage} from "../login/login";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  constructor(public nav: NavController,
              public authServiceProvider: AuthServiceProvider) {
  }

  // logout
  logout() {
    // this.nav.setRoot(LoginPage);
    this.authServiceProvider.logout().subscribe(
      res => {
        this.nav.setRoot(LoginPage);
      }
    )
  }
}
