import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { IonicModule, Platform, NavController} from 'ionic-angular/index';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {HomePage} from "./home";
import {PlatformMock, SplashScreenMock, StatusBarMock} from "../../../test-config/mocks-ionic";
import {Facebook} from "@ionic-native/facebook";
import {StorageMock} from "../../mock/main-providers/storage.mock";
import {TestApiService} from "../../providers/api/test.api.service";
import {FacebookMock} from "../../mock/main-providers/facebook.mock";
import {Storage} from "@ionic/storage";
import {TestsDaoService} from "../../providers/dao/tests.dao.service";

describe('HomePage', () => {
  let de: DebugElement;
  let comp: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomePage,
      ],
      imports: [
        IonicModule.forRoot(HomePage)
      ],
      providers: [
        NavController,
        TestApiService,
        TestsDaoService,
        {provide: StatusBar, useClass: StatusBarMock},
        {provide: SplashScreen, useClass: SplashScreenMock},
        {provide: Platform, useClass: PlatformMock},
        {provide: Facebook, useClass: FacebookMock},
        {provide: Storage, useClass: StorageMock}
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePage);
    comp = fixture.componentInstance;
  });

  it('should create component', () => {
    expect(comp).toBeDefined()
  });

  it('array of menu items should be created', () => {
    expect(comp.appMenuItems).toBeDefined('we define this array right in component');
    expect(Array.isArray(comp.appMenuItems)).toBeTruthy('as array');
  });

  it('constructor should create injected services', () => {
    expect(comp.nav).toBeDefined('NavController');
    expect(comp.nav instanceof NavController).toBeTruthy('Checked instance of NavController');
    expect(comp.testDaoService).toBeDefined('TestsDaoService');
    expect(comp.testDaoService instanceof TestsDaoService).toBeTruthy('Checked instance of TestsDaoService');
  });

  it('dom collection of cards',() => {
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css('ion-content.animated.fadeIn.common-bg'));
    expect(de.children.length).toBe(comp.appMenuItems.length, 'created card from array');

    const nativeElement: HTMLElement = de.nativeElement;
    comp.appMenuItems.forEach(item => {
      expect(nativeElement.innerHTML).toContain(item.title, 'Title is in html');
      expect(nativeElement.innerHTML).toContain(item.icon, 'Title is in html');
      expect(nativeElement.innerHTML).toContain(item.img, 'Title is in html');
    });
  });
});
