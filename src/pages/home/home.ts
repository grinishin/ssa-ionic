import {Component, OnInit} from "@angular/core";
import {NavController} from "ionic-angular";

import {TestsDaoService} from "../../providers/dao/tests.dao.service";
import {TestsPage} from "../tests/tests";
import {LocalWeatherPage} from "../local-weather/local-weather";
import {MenuItem} from "../../models/common";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage implements OnInit {

  appMenuItems: MenuItem[] = [
    {title: 'Tests', component: TestsPage, icon: 'checkmark-circle-outline', img: 'https://herefordisdquicktips.wikispaces.com/file/view/student-taking-test.jpg/142686555/560x308/student-taking-test.jpg'},
    {title: 'Local Weather', component: LocalWeatherPage, icon: 'partly-sunny', img: 'https://politeka.net/wp-content/uploads/2017/07/yasnaya-pogoda-1-900x600.jpg'}
  ];

  constructor(public nav: NavController,
              public testDaoService: TestsDaoService) {
  }

  ngOnInit() {

  }
  goTo(menuItem){
    this.nav.push(menuItem.component);
  }
}

//
