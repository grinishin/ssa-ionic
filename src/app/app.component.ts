import { Component, ViewChild } from "@angular/core";
import { Platform, Nav } from "ionic-angular";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { LocalWeatherPage } from "../pages/local-weather/local-weather";
import {AuthServiceProvider} from "../providers/auth-service/auth-service";
import {TestApiService} from "../providers/api/test.api.service";
import {TestsPage} from "../pages/tests/tests";
import {MenuItem} from "../models/common";


@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  appMenuItems: MenuItem[];

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public TestApiService: TestApiService,
    public keyboard: Keyboard,
    public authServiceProvider: AuthServiceProvider
  ) {
    this.initializeApp();
    this.TestApiService.getTestList();

    this.appMenuItems = [
      {title: 'Home', component: HomePage, icon: 'home'},
      {title: 'Tests', component: TestsPage, icon: 'checkmark-circle-outline'},
      {title: 'Local Weather', component: LocalWeatherPage, icon: 'partly-sunny'}
    ];
  }

  initializeApp() {
    console.log('initializeApp')
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.

      //*** Control Splash Screen
      // this.splashScreen.show();
      // this.splashScreen.hide();
      this.setRootPage();

      //*** Control Status Bar
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);

      //*** Control Keyboard
      this.keyboard.disableScroll(true);
    });
  }

  private setRootPage(){
    this.authServiceProvider.loginStatus.subscribe(
      res => {
        this.rootPage = res ? HomePage : LoginPage
      }
    )
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    // this.nav.setRoot(LoginPage);
    this.authServiceProvider.logout().subscribe(
      res => {
        this.nav.setRoot(LoginPage);
      }
    )
  }

}
