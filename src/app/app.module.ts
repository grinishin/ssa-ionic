import {NgModule} from "@angular/core";
import {IonicApp, IonicModule} from "ionic-angular";
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard';

import {ActivityService} from "../services/activity-service";
import {TripService} from "../services/trip-service";
import {WeatherProvider} from "../services/weather";

import {MyApp} from "./app.component";

import {SettingsPage} from "../pages/settings/settings";
import {CheckoutTripPage} from "../pages/checkout-trip/checkout-trip";
import {HomePage} from "../pages/home/home";
import {LoginPage} from "../pages/login/login";
import {NotificationsPage} from "../pages/notifications/notifications";
import {RegisterPage} from "../pages/register/register";
import {SearchLocationPage} from "../pages/search-location/search-location";
import {TripDetailPage} from "../pages/trip-detail/trip-detail";
import {TripsPage} from "../pages/trips/trips";
import {LocalWeatherPage} from "../pages/local-weather/local-weather";
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import {Facebook} from "@ionic-native/facebook";
import {HttpModule} from "@angular/http";
import {TestApiService} from "../providers/api/test.api.service";
import {TestsDaoService} from "../providers/dao/tests.dao.service";
import {TestCreatingPage} from "../pages/test-creating/test-creating";
import {TestPassPage} from "../pages/test-pass/test-pass";
import {ResultComponent} from "../pages/test-pass/result/result.component";
import {LongPressModule} from "ionic-long-press";
import {TestsPage} from "../pages/tests/tests";
import {SortPipe} from "../pages/tests/sort.pipe";

// import services
// end import services
// end import services

// import pages
// end import pages

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    TestCreatingPage,
    TestPassPage,
    TestsPage,
    ResultComponent,


    SettingsPage,
    CheckoutTripPage,
    LocalWeatherPage,
    NotificationsPage,
    RegisterPage,
    SearchLocationPage,
    TripDetailPage,
    TripsPage,

    SortPipe
  ],
  imports: [
    HttpModule,
    BrowserModule,
    LongPressModule,
    HttpClientModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    TestCreatingPage,
    TestPassPage,
    TestsPage,


    SettingsPage,
    CheckoutTripPage,
    LocalWeatherPage,
    NotificationsPage,
    RegisterPage,
    SearchLocationPage,
    TripDetailPage,
    TripsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    ActivityService,
    TripService,
    WeatherProvider,
    AuthServiceProvider,
    TestApiService,
    Facebook,
    TestsDaoService
  ]
})

export class AppModule {
}
