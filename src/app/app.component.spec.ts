import 'core-js/es6';
// import 'core-js/es5';
import 'core-js/shim';
import 'zone.js/dist/zone.js';
import 'zone.js/dist/long-stack-trace-zone';
import 'zone.js/dist/sync-test';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';
import 'zone.js/dist/proxy.js';
import 'zone.js/dist/jasmine-patch';
import 'reflect-metadata';
import 'zone.js/dist/fake-async-test';

import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {IonicModule, MenuController, NavController, Platform} from 'ionic-angular';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Storage} from '@ionic/storage';

import {MyApp} from './app.component';
import {
  PlatformMock,
  StatusBarMock,
  SplashScreenMock
} from '../../test-config/mocks-ionic';
import {TestApiService} from "../providers/api/test.api.service";
import {Keyboard} from "@ionic-native/keyboard";
import {AuthServiceProvider} from "../providers/auth-service/auth-service";
import {LoginPage} from "../pages/login/login";
import {StorageMock} from "../mock/main-providers/storage.mock";
import {Facebook} from "@ionic-native/facebook";
import {FacebookMock} from "../mock/main-providers/facebook.mock";
import {HomePage} from "../pages/home/home";
import {BrowserDynamicTestingModule} from "@angular/platform-browser-dynamic/testing";
import {AppModule} from "./app.module";
import {DebugElement, NgModule} from "@angular/core";
import {TestsDaoService} from "../providers/dao/tests.dao.service";

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {By} from "@angular/platform-browser";
import {SortPipe} from "../pages/tests/sort.pipe";
@NgModule({


  imports: [
    // AppModule
  ],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage
  ]
})
class MockAppModule {

}


describe('MyApp Component', () => {
  let de: DebugElement;
  let component: MyApp;
  let fixture: ComponentFixture<MyApp>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MyApp,
        HomePage,
        LoginPage,

        SortPipe
      ],
      providers: [
        TestApiService,
        MenuController,
        TestsDaoService,
        {provide: StatusBar, useClass: StatusBarMock},
        {provide: SplashScreen, useClass: SplashScreenMock},
        {provide: Platform, useClass: PlatformMock},
        {provide: Facebook, useClass: FacebookMock},
        {provide: Storage, useClass: StorageMock},
        Keyboard,
        AuthServiceProvider
      ],
      imports: [
        MockAppModule,
        IonicModule.forRoot(MyApp, {
          scrollPadding: false,
          scrollAssist: true,
          autoFocusAssist: false
        })
      ],

    })
  }));

  // TestBed.overrideModule(IonicModule, {
  //   set: {
  //     entryComponents: [
  //       HomePage,
  //       LoginPage
  //     ]
  //   }
  // });
  beforeEach(() => {
    fixture = TestBed.createComponent(MyApp);
    component = fixture.componentInstance;
    // de = fixture.debugElement.query(By.css('h3'));
  });

  it('should be created', () => {
    // expect(component instanceof MyApp).toBe(true);
    // expect(component.rootPage).toBeDefined();
    // expect(component.rootPage.name).toBe('HomePage');
  });

  it('component.setRootPage()', () => {
    // fixture.detectChanges();
    // console.log(Object.keys(fixture.__proto__));
    // console.log(component.rootPage.name);
    // expect(component.rootPage.name).toBe(HomePage.name);
    // component.setRootPage();
    // tick(1000);
    // expect(component.rootPage.name).toBe(LoginPage.name);
  });

});
