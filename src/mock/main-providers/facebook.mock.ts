import {Injectable} from "@angular/core";
import {User} from "../../models/user";
import {FacebookLoginResponse} from "@ionic-native/facebook";
import {facebook_login_response, user} from "../data-to-mock";

@Injectable()
export class FacebookMock {
  private _user: User;

  constructor() {

  }

  logout(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._user = null;
      resolve()
    })
  }

  login(): Promise<FacebookLoginResponse> {
    return new Promise((resolve, reject) => {
      this._user = user;
      resolve(facebook_login_response)
    })
  }

  getLoginStatus(): Promise<any> {
    return new Promise((resolve, reject) => {
      let response = {
        status: this._user ? 'connected' : null,
        user: this._user
      };
      resolve(response)
    })
  }

  api(path: string, permission: any[]): Promise<any> {
    return new Promise((resolve, reject) => {
      let userData: any = Object.assign({
        picture_large: {
          data: {
            url: user.pictureUrl
          }
        },
        first_name: user.firstName
      }, user);
      delete userData.pictureUrl;
      resolve(userData)
    })
  }

}
