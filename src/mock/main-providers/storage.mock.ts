

export class StorageMock {

  private _mockStorage: any = {};

  get(key: string): Promise<any>{
    return new Promise((resolve, reject) => {
      resolve(this._mockStorage[key])
    })
  }

  set(key: string, value: any): Promise<any>{
    this._mockStorage[key] = value;
    return new Promise((resolve, reject) => {
      resolve(value)
    })
  }

  remove(key: string): Promise<any>{
    return new Promise((resolve, reject) => {
      if (this._mockStorage.hasOwnProperty(key)){
        delete this._mockStorage[key];
        resolve()
      } else {
        reject(new Error())
      }
    })
  }

  clear(): Promise<any>{
    return new Promise((resolve, reject) => {
      this._mockStorage = {};
      resolve()
    })
  }

}
