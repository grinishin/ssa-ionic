import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';
import {Test} from '../../models/test';
import {StorageMock} from "../main-providers/storage.mock";

const API_KEYS = {
  all_tests: 'alltests'
};

@Injectable()
export class TestApiServiceMock {

  constructor(
    private storage: StorageMock
  ){
  }

  getTestList(): Observable<any> {
    return Observable.fromPromise(this.storage.get(API_KEYS.all_tests)).map(res => JSON.parse(res))
  }

  addTest(test: Test) {
    return this.getTestList().map(
      tests => {
        const testList: Test[] = tests || [];

        switch (testList.length) {
          case 0:
            test.id = 0;
            break;
          case 1:
            test.id = testList[0].id + 1;
            break;
          default:
            test.id = testList.sort((a, b) => {
              return a.id > b.id ? -1 : 1
            })[0].id + 1;
        }
        testList.push({
          id: test.id,
          name: test.name,
          questionsCount: test.questions.length,
          dateAdded: test.dateAdded
        });

        console.log(test);
        this.storage.set(API_KEYS.all_tests, JSON.stringify(testList));
        this.storage.set(`${test.id}`, JSON.stringify(test));

        return test
      }
    );


  }

  updateTest(test: Test) {
    return this.getTestList().map(
      tests => {
        const testList: Test[] = tests || [];
        const index = testList.findIndex(item => item.id == test.id);
        testList[index] = test;

        this.storage.set(API_KEYS.all_tests, JSON.stringify(testList));

        this.storage.set(`${test.id}`, JSON.stringify(test));
        return test
      }
    )
  }

  deleteTest(id: number){
    return this.getTestList().map(
      tests => {
        const testList: Test[] = tests || [];
        const index = testList.findIndex(item => item.id == id);
        testList.splice(index, 1);

        this.storage.set(API_KEYS.all_tests, JSON.stringify(testList));

        localStorage.removeItem(`${id}`);
        return id
      }
    )
  }

  getTest(id: number){
    return Observable.fromPromise(this.storage.get(`${id}`)).map(res => JSON.parse(res))
  }

}
