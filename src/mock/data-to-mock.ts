import {Question, QuestionType, Test} from "../models/test";
import {FacebookLoginResponse} from "@ionic-native/facebook";
import {User} from "../models/user";

export const fakeTest_1: Test = {
    name: 'test 1',
    dateAdded: (new Date()).toDateString(),
    questions: []
  },
  fakeTest_2: Test = {
    name: 'test 2',
    dateAdded: (new Date()).toDateString(),
    questions: []
  },
  fakeTest_3: Test = {
    name: 'test 3',
    dateAdded: (new Date()).toDateString(),
    questions: []
  };


export const question_1: Question = {
  questionText: 'Question 1',
  questionType: QuestionType.MULTI,
  answers: [
    {
      body: 'answer 1',
      correctness: true
    }
  ]
};

export const facebook_login_response: FacebookLoginResponse = {
  status: '200',
  authResponse: {
    session_key: true,
    accessToken: 'looooong token',
    expiresIn: 1,
    sig: 'sig',
    secret: 'secret',
    userID: '123456789',
  }
};

export const user: User = {
  email: 'ogrinishin@gmail.com',
  id: '1717694174961765',
  pictureUrl: 'https://graph.facebook.com/1717694174961765/picture?type=normal',
  firstName: 'Oleg',
  lastName: 'Grinishyn',
};
