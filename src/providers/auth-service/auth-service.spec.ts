import {TestBed, fakeAsync, tick} from '@angular/core/testing';
import {Storage} from "@ionic/storage";
import {StorageMock} from "../../mock/main-providers/storage.mock";
import 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/withLatestFrom';
import {AuthServiceProvider} from "./auth-service";
import {Platform} from "ionic-angular";
import {PlatformMock} from '../../../test-config/mocks-ionic';
import {Facebook, FacebookLoginResponse} from "@ionic-native/facebook";
import {FacebookMock} from "../../mock/main-providers/facebook.mock";
import {facebook_login_response, user} from "../../mock/data-to-mock";


describe("TestApiService", () => {
  let authServiceProvider: AuthServiceProvider;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthServiceProvider,
        {provide: Facebook, useClass: FacebookMock},
        {provide: Platform, useClass: PlatformMock},
        {provide: Storage, useClass: StorageMock},
      ]
    });
    authServiceProvider = TestBed.get(AuthServiceProvider);
  });

  it('Should create instance', () => {
    expect(authServiceProvider instanceof AuthServiceProvider).toBeTruthy()
  });

  it('AuthServiceProvider.login() && AuthServiceProvider.loginFromDevice() && AuthServiceProvider.getUserData()', fakeAsync(() => {
    authServiceProvider.login().subscribe(
      (res: FacebookLoginResponse) => {
        expect(JSON.stringify(res)).toBe(JSON.stringify(facebook_login_response));
      }
    );
    tick(1000);
    expect(authServiceProvider.user.pictureUrl).toBe(user.pictureUrl);
    expect(authServiceProvider.user.lastName).toBe(user.lastName);
    expect(authServiceProvider.user.firstName).toBe(user.firstName);
    expect(authServiceProvider.user.email).toBe(user.email);
    expect(authServiceProvider.user.id).toBe(user.id);
  }));

  it('AuthServiceProvider.logout() && AuthServiceProvider.logoutFromDevice() after AuthServiceProvider.login()', fakeAsync(() => {
    authServiceProvider.login().subscribe();
    tick(1000);
    authServiceProvider.logout().subscribe(
      res => {
        expect(authServiceProvider.user).toBeUndefined();
      }
    );
  }));

  it('AuthServiceProvider.login() && AuthServiceProvider.loginStatus()', fakeAsync(() => {
    authServiceProvider.login().subscribe();
    tick(1000);
    authServiceProvider.loginStatus.subscribe(
      res => {
        expect(res).toBeTruthy()
      }
    );
    tick(1000);
  }));
});
