import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Platform} from "ionic-angular";
import {User} from "../../models/user";
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import {Facebook} from "@ionic-native/facebook";
import {Observable} from "rxjs/Observable";
import {Storage} from '@ionic/storage';
import {user} from "../../mock/data-to-mock";

@Injectable()
export class AuthServiceProvider {

  private _user: User;

  constructor(private platform: Platform,
              private facebook: Facebook,
              private storage: Storage) {

  }

  set user(user: User) {
    // console.log(user);
    this._user = user;
  }

  get user(): User {
    return this._user
  }

  /**
   * login of user
   * @returns {any}
   */
  login() {
    if (this.platform.is('cordova')) {
      return this.loginFromDevice();
    } else {
      return this.mockLogin();
    }
  }

  /**
   * Logout of user
   * @returns {Observable<any>}
   */
  logout() {
    if (this.platform.is('cordova')) {
      return this.logoutFromDevice();
    } else {
      return this.mockLogout();
    }
  }

  /**
   * Fake request to logout to work from browser without cordova
   * @returns {Observable<{}>}
   */
  private mockLogout(): Observable<{}> {
    return Observable.of({}).delay(500).do(
      res => {
        this.storage.remove('user').then();
        this.user = undefined;
      }
    )
  }

  /**
   * Create request to logout user from facebook
   * @returns {Observable<any>}
   */
  private logoutFromDevice(): Observable<any> {
    return Observable.fromPromise(this.facebook.logout()).do(
      res => {
        this.user = undefined
      }
    )
  }

  /**
   * Create request to login user to facebook
   * @returns {Observable<any>}
   */
  private loginFromDevice(): Observable<any> {
    return Observable.fromPromise(this.facebook.login(['public_profile', 'user_friends', 'email'])).do(
      res => {
        this.getUserData()
      }
    )
  }

  /**
   * Fake request to login to work from browser without cordova
   * @returns {Observable<{}>}
   */
  private mockLogin(): Observable<{}> {
    return Observable.fromPromise(this.storage.set('user', JSON.stringify(user))).delay(500).do(
      res => {
        this.user = user;
      }
    )
  }

  /**
   * Creates request to facebook API to get specific user data
   * Set _user data
   */
  private getUserData(): void {
    this.facebook.api(`me?fields=id,email,first_name,last_name,picture.width(720).height(720).as(picture_large)`, []).then(
      profile => {
        this.user = {
          email: profile['email'],
          id: profile['id'],
          firstName: profile['first_name'],
          pictureUrl: profile['picture_large']['data']['url'],
          lastName: profile['lastName'],
        };
      }
    )
  }

  /**
   * Return Observer with login status
   * @returns {Observable<boolean>}
   */
  get loginStatus(): Observable<boolean> {
    if (this.platform.is('cordova')) {
      return this.loginStatusFromDevice();
    } else {
      return this.mockLoginStatusFromDevice();
    }
  }

  private loginStatusFromDevice(): Observable<boolean>{
    return Observable.fromPromise(this.facebook.getLoginStatus()).map(
      val => {
        return val.status == 'connected'
      }
    )
  }

  private mockLoginStatusFromDevice(): Observable<boolean>{
    return Observable.fromPromise(this.storage.get('user')).map(res => {
      this.user = JSON.parse(res);
      return Boolean(this.user)
    }).delay(500)
  }

}
