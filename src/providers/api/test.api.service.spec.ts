import 'core-js/es6';
// import 'core-js/es5';
import 'core-js/shim';
import 'zone.js/dist/zone.js';
import 'zone.js/dist/long-stack-trace-zone';
import 'zone.js/dist/sync-test';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';
import 'zone.js/dist/proxy.js';
import 'zone.js/dist/jasmine-patch';
import 'reflect-metadata';
import 'zone.js/dist/fake-async-test';

import {inject, TestBed, fakeAsync, tick, async} from '@angular/core/testing';
import {Storage} from "@ionic/storage";
import {StorageMock} from "../../mock/main-providers/storage.mock";
import {Answer, QuestionType, Test} from "../../models/test";
import {TestApiService} from "./test.api.service";
import 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/withLatestFrom';
import {fakeTest_1, fakeTest_2, fakeTest_3, question_1} from "../../mock/data-to-mock";


describe("TestApiService", () => {
  let testApiService: TestApiService

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TestApiService,
        {provide: Storage, useClass: StorageMock},
      ]
    });
    testApiService = TestBed.get(TestApiService);
  });

  it('Should create instance', () => {
    expect(testApiService instanceof TestApiService).toBeTruthy()
  });

  it('TestApiService.addTest() one test', async(() => {
    testApiService.addTest(fakeTest_1).subscribe(res => {
      expect(res.id).toBe(0);
      expect(res.questionsCount).toBe(fakeTest_1.questions.length);

      let resCopy: Test = Object.assign({}, res);
      delete resCopy.questionsCount;
      delete resCopy.id;
      expect(JSON.stringify(fakeTest_1)).toBe(JSON.stringify(resCopy));
    });
  }));


  it('TestApiService.addTest() after deleting one of previous test id should be +1 of the last', fakeAsync(() => {
    testApiService.addTest(fakeTest_1).subscribe();
    tick();
    testApiService.addTest(fakeTest_2).subscribe();
    tick();
    testApiService.deleteTest(0).subscribe();
    tick();
    testApiService.addTest(fakeTest_3).subscribe(
      (res: Test) => {
        expect(res.id).toBe(2)
      }
    );
    tick();
  }));


  it('TestApiService.deleteTest()', fakeAsync(() => {
    const IdToDelete: number = 1;
    testApiService.deleteTest(IdToDelete).subscribe(
      res => {
        expect(res).toBe(IdToDelete);
      }
    );
    tick();
    testApiService.addTest(fakeTest_1).subscribe();
    tick();
    testApiService.addTest(fakeTest_2).subscribe();
    tick();
    testApiService.addTest(fakeTest_3).subscribe();
    tick();
    testApiService.deleteTest(0).subscribe();
    tick();
  }));


  it('TestApiService.getTestList()', fakeAsync(() => {
    testApiService.getTestList().subscribe(
      (res: Test[]) => {
        expect(res.length).toBe(0);
        expect(res[0]).toBeUndefined();
      }
    );
    tick();
    testApiService.addTest(fakeTest_1).subscribe();
    tick();
    testApiService.addTest(fakeTest_2).subscribe();
    tick();
    testApiService.addTest(fakeTest_3).subscribe();
    tick();
    testApiService.getTestList().subscribe(
      (res: Test[]) => {
        expect(res.length).toBe(3);
        expect(res[0]).toBeDefined();
      }
    );
    tick();
  }));


  it('TestApiService.updateTest()', fakeAsync(() => {
    let savedTest: Test;
    testApiService.addTest(fakeTest_1).subscribe(
      res => {
        savedTest = res;
      }
    );
    tick();

    const toChangeInTest = {
        minCorrectAnswers: 1,
        questions: [
          question_1
        ]
      },
      newTest = Object.assign(toChangeInTest, savedTest);

    testApiService.updateTest(newTest).subscribe(
      (res: Test) => {
        expect(JSON.stringify(res.questions[0])).toBe(JSON.stringify(toChangeInTest.questions[0]));
        expect(res.minCorrectAnswers).toBe(toChangeInTest.minCorrectAnswers);
      }
    );
    tick();
  }));

  it('TestApiService.getTest()', fakeAsync(() => {
    testApiService.addTest(fakeTest_1).subscribe();
    tick();
    testApiService.getTest(0).subscribe(
      (res: Test) => {
        let resCopy: Test = Object.assign({}, res);
        delete resCopy.questionsCount;
        delete resCopy.id;
        expect(JSON.stringify(fakeTest_1)).toBe(JSON.stringify(resCopy));
      }
    );
    tick();
  }));
});
