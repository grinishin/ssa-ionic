import {TestBed, fakeAsync, tick} from '@angular/core/testing';
import {Storage} from "@ionic/storage";
import {StorageMock} from "../../mock/main-providers/storage.mock";
import {Test} from "../../models/test";
import 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/withLatestFrom';
import {fakeTest_1} from "../../mock/data-to-mock";
import {TestsDaoService} from "./tests.dao.service";
import {TestApiService} from "../api/test.api.service";


describe("TestApiService", () => {
  let testsDaoService: TestsDaoService;

  let checkTest = (res: Test) => {
    expect(res.name).toBe(fakeTest_1.name);
    expect(res.dateAdded).toBe(fakeTest_1.dateAdded);
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TestsDaoService,
        TestApiService,
        {provide: Storage, useClass: StorageMock},
      ]
    });
    testsDaoService = TestBed.get(TestsDaoService);
  });

  it('Should create instance', () => {
    expect(testsDaoService instanceof TestsDaoService).toBeTruthy()
  });

  it('testsDaoService.createTest()', fakeAsync(() => {
    testsDaoService.createTest(fakeTest_1).subscribe(
      res => {
        checkTest(res);
        expect(res.id).toBe(0);
      }
    );
    tick();
  }));

  it(', testsDaoService.getTest(), testsDaoService.getTestList()', fakeAsync(() => {
    let test: Test;
    testsDaoService.createTest(fakeTest_1).subscribe(
      res => {
        test = res;
      }
    );
    tick();

    testsDaoService.getTest(test.id).subscribe(
      res => {
        checkTest(res)
      }
    );

    testsDaoService.getTestList().subscribe(
      res => {
        checkTest(res[0])
      }
    );
  }));
});
