import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import {Test} from "../../models/test";
import {TestApiService} from "../api/test.api.service";

@Injectable()
export class TestsDaoService {
  constructor(
    private testService: TestApiService
  ){
    this.getTestList().subscribe()
  }
  private _tests: Test[] = [];

  set tests(test: Test[]){
    this._tests = test;
  }

  get tests(): Test[]{
    return this._tests.sort((test1, test2) => test1.name.toLowerCase() > test2.name.toLowerCase() ? 1 : -1)
  }

  getTestList(){
    return this.testService.getTestList().do(
      res => {
        this.tests = res ? res : [];
      }
    )
  }

  getTest(id: number): Observable<Test> {
    return this.testService.getTest(id)
  }

  createTest(test: Test) {
    return this.testService.addTest(test).do(
      res => {
        const newTests = this._tests.slice();
        newTests.push(res);
        this.tests = newTests;
      }
    )
  }

  updateTest(test: Test){
    return this.testService.updateTest(test).do(
      res => {
        const index = this._tests.findIndex(item => item.id == res.id);
        const newTests = this._tests.slice();
        newTests[index] = res;
        this.tests = newTests;
      }
    )
  }

  deleteTest(id: number){
    return this.testService.deleteTest(id).do(
      id => {
        this.tests = this._tests.slice().filter(item => item.id != id);
      }
    )
  }
}
