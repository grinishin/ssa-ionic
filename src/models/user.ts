export interface User {
  firstName: string;
  lastName: string;
  id: string;
  email: string;
  pictureUrl: string
}
