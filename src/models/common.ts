export interface MenuItem {
  title: string;
  component: any;
  icon: string;
  img? : string;
}
